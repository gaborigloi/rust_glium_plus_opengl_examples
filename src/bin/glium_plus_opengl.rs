// modified code from glium examples: https://github.com/tomaka/glium
// (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
// license: https://github.com/tomaka/glium/blob/master/LICENSE

#[macro_use]
extern crate glium;
extern crate gl;
extern crate image;

use std::io::Cursor;

use glium::GlObject;

fn main() {
    use glium::{DisplayBuild, Surface};

    let display = glium::glutin::WindowBuilder::new()
                      .with_dimensions(800, 600)
                      .build_glium()
                      .unwrap();

    // load the function pointers:
    let window = display.get_window().unwrap();
    unsafe {
        display.exec_in_context(|| {
            gl::load_with(|s| window.get_proc_address(s) as *const _);
        });
    }

    let texture = {
        let image = image::load(Cursor::new(&include_bytes!("../../resources/parrot.png")[..]),
                                image::PNG)
                        .unwrap()
                        .to_rgba();
        let image_dimensions = image.dimensions();
        let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(),
                                                                       image_dimensions);
        // We use SrgbTexture2d, not Texture2d to create an sRGB texture,
        // because the image is in sRGB.
        glium::texture::SrgbTexture2d::new(&display, image).unwrap()
    };

    // can call OpenGL functions:
    unsafe {
        display.exec_in_context(|| {
            // save the original OpenGL state
            let mut orig: gl::types::GLint = 0;
            gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &mut orig as *mut _);
            // manipulate OpenGL state:
            gl::BindTexture(gl::TEXTURE_2D, texture.get_id());
            gl::GenerateMipmap(gl::TEXTURE_2D);
            // restore the OpenGL state:
            gl::BindTexture(gl::TEXTURE_2D, orig as u32);
        });
    }

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex {
            position: [-1.0, -1.0],
            tex_coords: [0.0, 0.0],
        };
        let vertex2 = Vertex {
            position: [-1.0, 1.0],
            tex_coords: [0.0, 1.0],
        };
        let vertex3 = Vertex {
            position: [1.0, -1.0],
            tex_coords: [1.0, 0.0],
        };
        let vertex4 = Vertex {
            position: [1.0, 1.0],
            tex_coords: [1.0, 1.0],
        };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out vec4 color;

            uniform sampler2D tex;

            void main() {
                color = texture(tex, v_tex_coords);
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let uniforms = uniform! { tex: &texture };


    loop {
        let mut target = display.draw();

        target.clear_color(0.0, 0.0, 1.0, 1.0);
        target.draw(&vertex_buffer,
                    &indices,
                    &program,
                    &uniforms,
                    &Default::default())
              .unwrap();
        target.finish().unwrap();

        for ev in display.poll_events() {
            match ev {
                glium::glutin::Event::Closed => return,
                _ => (),
            }
        }
    }
}
