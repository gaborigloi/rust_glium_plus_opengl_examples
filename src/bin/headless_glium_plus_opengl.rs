// modified code from glium examples: https://github.com/tomaka/glium
// (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
// license: https://github.com/tomaka/glium/blob/master/LICENSE

#[macro_use]
extern crate glium;
extern crate gl;
extern crate image;

use std::io::Cursor;

use glium::GlObject;

fn main() {
    use glium::Surface;
    use glium::backend::Backend;

    let headless_backend = glium::glutin::HeadlessRendererBuilder::new(800, 600);
    let headless_backend =
        glium::backend::glutin_backend::GlutinHeadlessBackend::new(headless_backend).unwrap();
    unsafe {
        headless_backend.make_current();
        gl::load_with(|s| headless_backend.get_proc_address(s) as *const _);
    }
    let context = unsafe {
        // returns an Rc<Context>, for which Facade is implemented
        glium::backend::Context::new::<_, glium::glutin::CreationError>(headless_backend,
                                                                        false,
                                                                        Default::default())
        // (check_current_context:) We must ensure that no OpenGL context will be active in this thread,
        // if other context might be made current, this should be set to true
            .unwrap()
    };

    let texture = {
        let image = image::load(Cursor::new(&include_bytes!("../../resources/parrot.png")[..]),
                                image::PNG)
                        .unwrap()
                        .to_rgba();
        let image_dimensions = image.dimensions();
        let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(),
                                                                       image_dimensions);
        glium::texture::Texture2d::new(&context, image).unwrap()
    };

    let output = glium::Texture2d::empty(&context, 64, 64).unwrap();

    // can call OpenGL functions:
    unsafe {
        context.exec_in_context(|| {
            // save the original OpenGL state
            let mut orig: gl::types::GLint = 0;
            gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &mut orig as *mut _);
            // manipulate OpenGL state:
            gl::BindTexture(gl::TEXTURE_2D, texture.get_id());
            gl::GenerateMipmap(gl::TEXTURE_2D);
            // restore the OpenGL state:
            gl::BindTexture(gl::TEXTURE_2D, orig as u32);
        });
    }

    // draw using the Facade:

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex {
            position: [-1.0, -1.0],
            tex_coords: [0.0, 0.0],
        };
        let vertex2 = Vertex {
            position: [-1.0, 1.0],
            tex_coords: [0.0, 1.0],
        };
        let vertex3 = Vertex {
            position: [1.0, -1.0],
            tex_coords: [1.0, 0.0],
        };
        let vertex4 = Vertex {
            position: [1.0, 1.0],
            tex_coords: [1.0, 1.0],
        };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&context, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
                #version 110

                attribute vec2 position;
                attribute vec2 tex_coords;
                varying vec2 v_tex_coords;

                void main() {
                    v_tex_coords = tex_coords;
                    gl_Position = vec4(position, 0.0, 1.0);
                }
            "#;

        let fragment_shader_src = r#"
                #version 110

                varying vec2 v_tex_coords;

                uniform sampler2D tex;

                void main() {
                    gl_FragColor = texture2D(tex, v_tex_coords);
                }
            "#;

        glium::Program::from_source(&context, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let uniforms = uniform! { tex: &texture };

    output.as_surface()
          .draw(&vertex_buffer,
                &indices,
                &program,
                &uniforms,
                &Default::default())
          .unwrap();
}
